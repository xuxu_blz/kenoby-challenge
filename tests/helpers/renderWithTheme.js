import React from 'react'
import { ThemeProvider } from 'styled-components'
import enzyme from 'enzyme'

import { theme } from '../../src/styles/theme'

export const shallow = tree => {
  const context = enzyme
    .shallow(<ThemeProvider theme={theme} />)
    .instance()
    .getChildContext()

  return enzyme.shallow(tree, { context })
}

export const mount = tree => {
  const context = enzyme
    .shallow(<ThemeProvider theme={theme} />)
    .instance()
    .getChildContext()

  return enzyme.mount(tree, {
    context,
    childContextTypes: ThemeProvider.childContextTypes,
  })
}
