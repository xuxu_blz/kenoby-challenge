import mockAxios from 'jest-mock-axios'

import positionsResource from '../../src/resources/positions'
import { positions } from '../react/components/Positions/mockData'

afterEach(mockAxios.reset)

describe('Positions resource', () => {
  it('Promise resolves with position data on success', () => {
    const request = positionsResource.get()
    mockAxios.mockResponse({ data: positions })
    expect(request).resolves.toEqual([null, positions])
  })

  it('Promise resolves with error data on error', () => {
    const request = positionsResource.get()
    mockAxios.mockError({ response: { status: 408 } })
    expect(request).resolves.toEqual([
      { code: 408, message: 'Houve um erro no servidor' },
      [],
    ])
  })
})
