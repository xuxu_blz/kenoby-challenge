import React from 'react'
import { mount } from 'enzyme'

import { MockChild } from '../../helpers/components.js'
import { withResource } from '../../../src/react/HOCs/withResource'

const render = Component => mount(Component)

describe('WithResource HOC', function() {
  test('renders', function() {
    const ComponentWithResource = withResource(
      () => Promise.resolve([null, {}]),
      'testData'
    )(MockChild)

    expect(render(<ComponentWithResource />)).toBeDefined()
  })

  test('calls the resource function', function() {
    const fn = jest.fn()

    const ComponentWithResource = withResource(() => {
      fn()
      return Promise.resolve([null, {}])
    }, 'testData')(MockChild)

    render(<ComponentWithResource />)

    expect(fn).toBeCalled()
  })

  test('passes the state as a prop with the chosen name', function() {
    const ComponentWithResource = withResource(
      () => Promise.resolve([null, {}]),
      'testData'
    )(MockChild)

    const Component = render(<ComponentWithResource />).setState({
      data: { test: 'test' },
    })
    expect(Component.find(MockChild).prop('testData')).toEqual({ test: 'test' })
  })
})
