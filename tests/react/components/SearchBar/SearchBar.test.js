import React from 'react'
import { shallow } from '../../../helpers/renderWithTheme'

import SearchBar from '../../../../src/react/components/SearchBar'
import SearchInput from '../../../../src/react/primitives/SearchInput'
import Switch from '../../../../src/react/primitives/Switch'

const defaultProps = {
  searchString: '',
  showFavorites: false,
  onToggleFavorites: () => null,
  onUpdateSearchField: () => null,
}

describe('Positions List', () => {
  it('Renders', () => {
    expect(shallow(<SearchBar {...defaultProps} />)).toBeTruthy()
  })

  it('Renders passed string into the field value', () => {
    expect(
      shallow(<SearchBar {...defaultProps} searchString="test string" />)
        .find(SearchInput)
        .prop('value')
    ).toBe('test string')
  })

  it('Renders passed string into the field value', () => {
    expect(
      shallow(<SearchBar {...defaultProps} showFavorites={true} />)
        .find(Switch)
        .prop('active')
    ).toBe(true)
  })

  it('Fires onUpdateSearchField() when input changes', () => {
    const fn = jest.fn()

    const wrapper = shallow(
      <SearchBar {...defaultProps} onUpdateSearchField={fn} />
    )

    wrapper
      .find(SearchInput)
      .simulate('change', { target: { value: 'test string' } })

    expect(fn).toBeCalledWith('test string')
  })

  it('Fires onToggleFavorites() when the favorite switch is clicked', () => {
    const fn = jest.fn()

    const wrapper = shallow(
      <SearchBar {...defaultProps} onToggleFavorites={fn} />
    )

    wrapper.find(Switch).simulate('click')

    expect(fn).toBeCalled()
  })
})
