export const positions = [
  {
    name: 'sales development representative (pré-vendas)',
    description: '<p>descrição da vaga</p>',
    customfields: [
      { code: 'state', field: '5634a94bfba4241523febada', value: 'sp' },
      { code: 'city', field: '5634a94bfba4241523febadb', value: 'são paulo' },
      { code: 'segment', field: '5634a94bfba4241523febad2', value: 'vendas' },
    ],
    jobsites: [
      {
        _id: '57981bfcd104a18d551a8468',
        name: 'kenoby',
        slug: 'kenoby',
        picture:
          'https://s3-sa-east-1.amazonaws.com/prod-tenant-logos/55b7e031299d4e33019c1c5a',
        facebookpicture:
          'https://s3-sa-east-1.amazonaws.com/prod-jobsite-files/uploads/kenoby-1470144603-kenoby---logo-azul-quadrada.png',
        internal: false,
      },
    ],
    deadline: 1,
    openeddays: 0,
    jobsitesavailable: [
      { slug: 'kenoby', _id: '57981bfcd104a18d551a8468' },
      { slug: 'kenoby-labs', _id: '59386cdfd99b38bed2e85a9a' },
      { slug: 'kenoby-ri', _id: '59f8782f05dd601d14cbec32' },
      { slug: 'kenoby-en', _id: '5a96c2f3c26dc866523c41ed' },
      { slug: 'kenoby12', _id: '5af8d310b9dc8d3631d91bea' },
    ],
    linksInternal: [],
    linksExternal: [
      'http://jobs.kenoby.com/kenoby/job/sales-development-representative-pr-vendas/5a560ee6e9f6eb038f2029f2',
    ],
    _id: '1a560ee6e9f6eb038f2029f2',
  },
  {
    name: 'second sales development representative (pré-vendas)',
    description: '<p>descrição da vaga</p>',
    customfields: [
      { code: 'state', field: '5634a94bfba4241523febada', value: 'sp' },
      { code: 'city', field: '5634a94bfba4241523febadb', value: 'são paulo' },
      { code: 'segment', field: '5634a94bfba4241523febad2', value: 'vendas' },
    ],
    jobsites: [
      {
        _id: '57981bfcd104a18d551a8468',
        name: 'kenoby',
        slug: 'kenoby',
        picture:
          'https://s3-sa-east-1.amazonaws.com/prod-tenant-logos/55b7e031299d4e33019c1c5a',
        facebookpicture:
          'https://s3-sa-east-1.amazonaws.com/prod-jobsite-files/uploads/kenoby-1470144603-kenoby---logo-azul-quadrada.png',
        internal: false,
      },
    ],
    deadline: 1,
    openeddays: 0,
    jobsitesavailable: [
      { slug: 'kenoby', _id: '57981bfcd104a18d551a8468' },
      { slug: 'kenoby-labs', _id: '59386cdfd99b38bed2e85a9a' },
      { slug: 'kenoby-ri', _id: '59f8782f05dd601d14cbec32' },
      { slug: 'kenoby-en', _id: '5a96c2f3c26dc866523c41ed' },
      { slug: 'kenoby12', _id: '5af8d310b9dc8d3631d91bea' },
    ],
    linksInternal: [],
    linksExternal: [
      'http://jobs.kenoby.com/kenoby/job/sales-development-representative-pr-vendas/5a560ee6e9f6eb038f2029f2',
    ],
    _id: '6a560ee6e9f6eb038f2029f2',
  },
  {
    name: 'third sales development representative (pré-vendas)',
    description: '<p>descrição da vaga</p>',
    customfields: [
      { code: 'state', field: '5634a94bfba4241523febada', value: 'sp' },
      { code: 'city', field: '5634a94bfba4241523febadb', value: 'são paulo' },
      { code: 'segment', field: '5634a94bfba4241523febad2', value: 'vendas' },
    ],
    jobsites: [
      {
        _id: '57981bfcd104a18d551a8468',
        name: 'kenoby',
        slug: 'kenoby',
        picture:
          'https://s3-sa-east-1.amazonaws.com/prod-tenant-logos/55b7e031299d4e33019c1c5a',
        facebookpicture:
          'https://s3-sa-east-1.amazonaws.com/prod-jobsite-files/uploads/kenoby-1470144603-kenoby---logo-azul-quadrada.png',
        internal: false,
      },
    ],
    deadline: 1,
    openeddays: 0,
    jobsitesavailable: [
      { slug: 'kenoby', _id: '57981bfcd104a18d551a8468' },
      { slug: 'kenoby-labs', _id: '59386cdfd99b38bed2e85a9a' },
      { slug: 'kenoby-ri', _id: '59f8782f05dd601d14cbec32' },
      { slug: 'kenoby-en', _id: '5a96c2f3c26dc866523c41ed' },
      { slug: 'kenoby12', _id: '5af8d310b9dc8d3631d91bea' },
    ],
    linksInternal: [],
    linksExternal: [
      'http://jobs.kenoby.com/kenoby/job/sales-development-representative-pr-vendas/5a560ee6e9f6eb038f2029f2',
    ],
    _id: '5a560ee6e9f6eb038f2029f2',
  },
]
