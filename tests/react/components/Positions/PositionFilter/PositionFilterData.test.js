import {
  getFavorites,
  storeFavorites,
} from '../../../../../src/react/components/Positions/PositionFilter/data'

describe('Position Filter data storage', () => {
  beforeAll(() => {
    localStorage.clear()
  })

  afterEach(() => {
    localStorage.clear()
  })

  describe('getFavorites()', () => {
    it('returns an empty array when storage is empty', () => {
      expect(getFavorites()).toEqual([])
    })

    it('returns the favorites list if it is saved to localStorage', () => {
      const list = ['a', 'b', 'c']
      localStorage.setItem('kenoby_favorites', JSON.stringify(list))

      expect(getFavorites()).toEqual(list)
    })
  })

  describe('storeFavorites()', () => {
    it('saves the passed array to localStorage', () => {
      const list = ['d', 'e', 'f']
      storeFavorites(list)

      expect(JSON.parse(localStorage.getItem('kenoby_favorites'))).toEqual(list)
    })
  })
})
