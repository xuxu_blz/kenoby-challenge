import React from 'react'
import { shallow } from 'enzyme'
import { map } from 'ramda'

import PositionFilter from '../../../../../src/react/components/Positions/PositionFilter'
import { MockChild } from '../../../../helpers//components.js'
import { positions } from '../mockData'

const baseProps = {
  positions,
  searchString: '',
  showFavorites: false,
  favoritesList: [],
}

const renderChildren = map(position => <MockChild key={position._id} />)

describe('Positions List', () => {
  it('Renders', () => {
    expect(shallow(<PositionFilter {...baseProps} />)).toBeTruthy()
  })

  it('Renders all children when no filters are active', () => {
    expect(
      shallow(
        <PositionFilter {...baseProps}>{renderChildren}</PositionFilter>
      ).find(MockChild).length
    ).toBe(positions.length)
  })

  it('Filters positions based on the search string', () => {
    expect(
      shallow(
        <PositionFilter {...baseProps} searchString="second">
          {renderChildren}
        </PositionFilter>
      ).find(MockChild).length
    ).toBe(1)
  })

  it('Filters favorite positions when props are passed', () => {
    expect(
      shallow(
        <PositionFilter {...baseProps} showFavorites={true}>
          {renderChildren}
        </PositionFilter>
      )
        .setState({
          favoritesList: [
            '1a560ee6e9f6eb038f2029f2',
            '6a560ee6e9f6eb038f2029f2',
          ],
        })
        .find(MockChild).length
    ).toBe(2)
  })

  it('Filters favorite and search at the same time', () => {
    expect(
      shallow(
        <PositionFilter
          {...baseProps}
          showFavorites={true}
          searchString="second"
        >
          {renderChildren}
        </PositionFilter>
      )
        .setState({
          favoritesList: [
            '1a560ee6e9f6eb038f2029f2',
            '6a560ee6e9f6eb038f2029f2',
          ],
        })
        .find(MockChild).length
    ).toBe(1)
  })

  it('Updates state when handleToggleFavorite is called', () => {
    const wrapper = shallow(<PositionFilter {...baseProps} />).setState({
      favoritesList: ['abc', 'def', 'ghi'],
    })

    wrapper.instance().handleToggleFavorite('abc')
    expect(wrapper.state('favoritesList')).toEqual(['def', 'ghi'])

    wrapper.instance().handleToggleFavorite('abc')
    expect(wrapper.state('favoritesList')).toEqual(['def', 'ghi', 'abc'])
  })
})
