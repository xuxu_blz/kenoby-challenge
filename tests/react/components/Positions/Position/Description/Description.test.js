import React from 'react'
import { shallow } from 'enzyme'

import Description from '../../../../../../src/react/components/Positions/Position/Description'

const cleanHTML = '<h3>Some html</h3>'

const dirtyHTML = `
  <h3 dir="ltr" style="line-height: 1.8461538461538463; margin-top: 14pt;
  margin-bottom: 4pt;" data-mce-style="line-height: 1.8461538461538463;
  margin-top: 14pt; margin-bottom: 4pt;">
    <span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"
    data-mce-style="font-size: 10pt; font-family: arial, helvetica, sans-serif;">
      <strong><span style="color: rgb(46, 46, 46); white-space: pre-wrap;"
      data-mce-style="color: #2e2e2e; white-space: pre-wrap;">
        some html
      </span></strong>
    </span>
  </h3>`

describe('Positions List', () => {
  it('Renders', () => {
    expect(shallow(<Description html={cleanHTML} />)).toBeTruthy()
  })

  it('Renders received string as HTML', () => {
    expect(shallow(<Description html={cleanHTML} />).html()).toMatchSnapshot()
  })

  it('Cleans up HTML before rendering', () => {
    expect(shallow(<Description html={dirtyHTML} />).html()).toMatchSnapshot()
  })
})
