import React from 'react'
import { shallow } from 'enzyme'

import Position from '../../../../../src/react/components/Positions/Position'
import Description from '../../../../../src/react/components/Positions/Position/Description'
import Dropdown from '../../../../../src/react/primitives/Dropdown'
import FavoriteToggle from '../../../../../src/react/primitives/FavoriteToggle'
import { ActionButtonLink } from '../../../../../src/react/primitives/ActionButton'
import { positions } from '../mockData'

const baseProps = {
  position: positions[0],
  active: false,
  onToggle: () => undefined,
  onToggleFavorite: () => undefined,
}

describe('Positions List', () => {
  it('Renders', () => {
    expect(shallow(<Position {...baseProps} />)).toBeTruthy()
  })

  it('Passes its active state to the Dropdown component', () => {
    expect(
      shallow(<Position {...baseProps} active={true} />)
        .find(Dropdown)
        .prop('active')
    ).toBe(true)
  })

  it('Passes content to the Description component', () => {
    expect(
      shallow(<Position {...baseProps} active={true} />)
        .find(Dropdown)
        .dive()
        .find(Description)
        .prop('html')
    ).toBe(positions[0].description)
  })

  it('Adds a link with the url received from props', () => {
    expect(
      shallow(<Position {...baseProps} active={true} />)
        .find(Dropdown)
        .dive()
        .find(ActionButtonLink)
        .prop('href')
    ).toBe(positions[0].linksExternal[0])
  })

  it('Calls onToggle() function with id when title is clicked', () => {
    const mockFn = jest.fn()

    shallow(<Position {...baseProps} onToggle={mockFn} />)
      .find(Dropdown)
      .dive()
      .find('h2')
      .simulate('click', { target: 1, currentTarget: 1 })

    expect(mockFn.mock.calls[0][0]).toBe(positions[0]._id)
  })

  it('Calls onToggleFavorite() function with id when title is clicked', () => {
    const mockFn = jest.fn()

    shallow(<Position {...baseProps} onToggleFavorite={mockFn} />)
      .find(Dropdown)
      .dive()
      .find(FavoriteToggle)
      .simulate('click')

    expect(mockFn.mock.calls[0][0]).toBe(positions[0]._id)
  })

  it('Reflects favorite status on icon', () => {
    expect(
      shallow(
        <Position
          {...baseProps}
          position={{ ...baseProps.position, favorite: true }}
        />
      )
        .find(Dropdown)
        .dive()
        .find(FavoriteToggle)
        .prop('active')
    ).toBe(true)

    expect(
      shallow(
        <Position
          {...baseProps}
          position={{ ...baseProps.position, favorite: false }}
        />
      )
        .find(Dropdown)
        .dive()
        .find(FavoriteToggle)
        .prop('active')
    ).toBe(false)
  })
})
