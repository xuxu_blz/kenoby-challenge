import React from 'react'
import { shallow } from 'enzyme'

import Positions from '../../../../src/react/components/Positions'
import Position from '../../../../src/react/components/Positions/Position'
import PositionFilter from '../../../../src/react/components/Positions/PositionFilter'
import Error from '../../../../src/react/primitives/Error'
import Spinner from '../../../../src/react/primitives/Spinner'
import { positions } from './mockData'

const baseProps = {
  positions,
  searchString: '',
  showFavorites: false,
}

describe('Positions List', () => {
  it('Renders', () => {
    expect(shallow(<Positions {...baseProps} />)).toBeTruthy()
  })

  it('Renders spinner while positions is null', () => {
    expect(
      shallow(<Positions {...baseProps} positions={null} />).find(Spinner)
        .length
    ).toBe(1)
  })

  it('Renders error component when an error object is passed', () => {
    expect(
      shallow(
        <Positions
          {...baseProps}
          resourceError={{ code: 404, message: 'Test error message' }}
        />
      ).find(Error).length
    ).toBe(1)
  })

  it('Renders one position item per available position', () => {
    expect(
      shallow(<Positions {...baseProps} />)
        .find(PositionFilter)
        .dive()
        .find(Position).length
    ).toBe(positions.length)
  })

  it('Changes the selectedPosition in state when the toggle event is called', () => {
    const wrapper = shallow(<Positions {...baseProps} />)
    wrapper.instance().handleTogglePosition('abcd')
    expect(wrapper.state().selectedPosition).toBe('abcd')
  })

  it('Passes true to the active prop of the selected position', () => {
    const wrapper = shallow(<Positions {...baseProps} />)
    wrapper.setState({ selectedPosition: '1a560ee6e9f6eb038f2029f2' })

    expect(
      wrapper
        .find(PositionFilter)
        .dive()
        .find(Position)
        .at(0)
        .props().active
    ).toBeTruthy()
  })

  it('Passes its filtering props to the PositionFilter component', () => {
    const wrapper = shallow(
      <Positions
        {...baseProps}
        searchString="test string"
        showFavorites={true}
      />
    )
    wrapper.setState({ selectedPosition: '1a560ee6e9f6eb038f2029f2' })

    expect(wrapper.find(PositionFilter).props()).toMatchObject({
      positions,
      searchString: 'test string',
      showFavorites: true,
    })
  })
})
