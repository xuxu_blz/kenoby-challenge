import React from 'react'
import { shallow, mount } from '../../../helpers/renderWithTheme'

import Main from '../../../../src/react/pages/Main'
import Positions from '../../../../src/react/components/Positions'
import SearchBar from '../../../../src/react/components/SearchBar'

describe('Main Page', () => {
  it('Renders', () => {
    expect(shallow(<Main />)).toBeTruthy()
  })

  it('Includes Position list and Search Bar', () => {
    const wrapper = mount(<Main />)
    expect(wrapper.find(SearchBar)).toBeTruthy()
    expect(wrapper.find(Positions)).toBeTruthy()
  })

  it('Passes its state to children accordingly', () => {
    const searchString = 'test string'
    const showFavorites = true

    const wrapper = mount(<Main />).setState({ searchString, showFavorites })

    expect(wrapper.find(SearchBar).prop('searchString')).toBe(searchString)
    expect(wrapper.find(SearchBar).prop('showFavorites')).toBe(showFavorites)
    expect(wrapper.find(Positions).prop('searchString')).toBe(searchString)
    expect(wrapper.find(Positions).prop('showFavorites')).toBe(showFavorites)
  })

  it('Updates state when handleUpdateSearchField is called', () => {
    const wrapper = shallow(<Main />)
    wrapper.instance().handleUpdateSearchField('another test string')
    expect(wrapper.state('searchString')).toBe('another test string')
  })

  it('Updates state when handleToggleFavorites is called', () => {
    const wrapper = shallow(<Main />).setState({ showFavorites: false })
    wrapper.instance().handleToggleFavorites()
    expect(wrapper.state('showFavorites')).toBe(true)
  })
})
