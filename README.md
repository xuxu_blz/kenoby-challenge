# A personal React Starter project

## What the project does

Runs the frontend for a hypothetical app required for Kenoby's Front-end
developer test.

## Project dependencies

This project should run fine in any operational system. The required software
is:

* [Node.js](https://nodejs.org/en/)
* [Yarn](https://yarnpkg.com/en/)

## Running the project

Install all dependencies with Yarn:

```bash
yarn install
```

### Development mode

Run:

```bash
yarn start
```

### Production mode

Run:

```bash
yarn build
yarn serve
```

### Running with Docker-compose

This project uses a simple node-alpine docker image for development purposes.
You can quickly run the development version with docker compose with:

```bash
docker-compose up
```

And if you want to run the production version, you can use:

```bash
docker-compose run --rm web yarn build && yarn serve
```

## Technology used

- React
- Flow
- Jest
- Styled Components
- Webpack
