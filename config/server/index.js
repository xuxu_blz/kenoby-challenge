const express = require('express')
const app = express()
const path = require('path')

const distFolder = path.resolve(__dirname, '../../dist')

const port = process.env.PORT || 8080

app.use(express.static(distFolder))

app.get('/*', function(req, res) {
  res.sendFile(path.resolve(distFolder, 'index.html'))
})

app.listen(port)
console.log(`app is listening on http://localhost:${port}`)
