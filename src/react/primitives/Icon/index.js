// @flow strict
import styled from 'styled-components'

type IconProps = {
  fontSize: string,
  lineHeight: string,
  color: string,
}

const Icon = styled.i`
  font-size: ${({ fontSize }: IconProps) => (fontSize ? fontSize : '24px')};
  color: ${({ color }: IconProps) => (color ? color : 'inherit')};
  line-height: ${({ lineHeight }: IconProps) =>
    lineHeight ? lineHeight : '16px'};
  vertical-align: middle;
  font-family: 'Material Icons';
  font-weight: normal;
  font-style: normal;
  display: inline-block;
  text-transform: none;
  letter-spacing: normal;
  word-wrap: normal;
  white-space: nowrap;
  direction: ltr;
  -webkit-font-smoothing: antialiased;
  text-rendering: optimizeLegibility;
  -moz-osx-font-smoothing: grayscale;
  font-feature-settings: 'liga';
`

export default Icon
