// @flow strict
import React from 'react'
import styled from 'styled-components'

import Icon from '../Icon'
import { theme } from '../../../styles/theme.js'

const Outer = styled.span`
  display: inline-block;
  height: 36px;
  width: 36px;
  position: relative;
  cursor: pointer;

  &::before {
    content: '';
    display: block;
    transition: background 0.3s ease-in-out;
    background: ${({ theme, active }) =>
      active ? theme.primaryColor(4) : theme.primaryColor(0)};
    height: 14px;
    width: 100%;
    border-radius: 7px;
    position: absolute;
    top: 11px;
    left: 0;
  }
`

const Inner = styled.span`
  background: ${({ theme, active }) =>
    active ? theme.gray(2) : theme.primaryColor(2)};
  width: 20px;
  height: 20px;
  border-radius: 50%;
  transition: all 0.3s ease-in-out;
  position: absolute;
  top: 8px;
  left: ${({ active }) => (active ? '16px' : '0')};
  box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
  text-align: center;

  i {
    margin-top: -7px;
    transition: color 0.3s ease-in-out;
  }
`

type Props = {
  active: boolean,
  onClick: (SyntheticMouseEvent<HTMLButtonElement>) => void,
}

const Switch = ({ active, onClick }: Props) => (
  <Outer onClick={onClick} active={active}>
    <Inner active={active}>
      <Icon
        fontSize="15px"
        lineHeight="20px"
        color={active ? theme.primaryColor(1) : '#fff'}
      >
        star
      </Icon>
    </Inner>
  </Outer>
)

export default Switch
