// @flow strict
import styled from 'styled-components'

const Error = styled.div`
  border: 3px solid ${({ theme }) => theme.danger()};
  border-radius: 3px;
  font-size: 14px;
  padding: 20px 40px;
  max-width: 400px;
  margin: 0 auto;
  text-align: center;
`

export default Error
