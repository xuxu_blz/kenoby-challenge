// @flow strict
import React from 'react'
import styled from 'styled-components'

import Icon from '../Icon'
import { theme } from '../../../styles/theme.js'

const Outer = styled.span`
  display: inline-block;
  cursor: pointer;
`

type Props = {
  active?: boolean,
  onClick: (SyntheticMouseEvent<HTMLElement>) => void,
}

const FavoriteToggle = ({ active, onClick }: Props) => (
  <Outer onClick={onClick}>
    <Icon
      fontSize="20px"
      lineHeight="20px"
      color={active ? theme.yellow() : theme.gray(0)}
    >
      {active ? 'star' : 'star_border'}
    </Icon>
  </Outer>
)

export default FavoriteToggle
