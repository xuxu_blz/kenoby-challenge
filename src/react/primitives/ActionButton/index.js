// @flow strict
import styled from 'styled-components'

export const ActionButton = styled.button`
  display: inline-block;
  background: ${({ theme }) => theme.primaryColor(1)};
  border: none;
  border-radius: 2px;
  padding: 0.7em 1em;
  font-size: 14px;
  line-height: 1.2em;
  font-weight: 600;
  text-transform: uppercase;
  text-decoration: none;
  color: #fff;
  box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.3);
  cursor: pointer;
  transition: background 0.3s ease-in-out;

  &:hover {
    background: ${({ theme }) => theme.primaryColor(3)};
  }
`

export const ActionButtonLink = ActionButton.withComponent('a')
