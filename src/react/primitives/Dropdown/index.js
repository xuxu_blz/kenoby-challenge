// @flow strict
import * as React from 'react'
import styled from 'styled-components'

import Icon from '../Icon'

type Props = {
  children: { renderTitle: () => React.Node, renderBody: () => React.Node },
  active: boolean,
}

const Container = styled.article`
  margin: ${({ active }) => (active ? '20px 0' : '-1px 0 0')};
  background: #fff;
  border: 1px solid ${({ theme }) => theme.gray(1)};
  border-radius: 3px;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2);

  &:first-child {
    margin-top: 0;
  }
`

const Header = styled.header`
  position: relative;

  h2 {
    font-size: 14px;
    padding: 20px;
    cursor: pointer;
  }
`

const Arrow = Icon.extend`
  position: absolute;
  right: 16px;
  top: 22px;
  pointer-events: none;
  color: ${({ theme }) => theme.gray()};
`

const Content = styled.section`
  transition: height 0.3s ease-in-out;
  overflow: hidden;
  padding: 0 20px 20px;
`

class Dropdown extends React.PureComponent<Props> {
  render() {
    const { children, active } = this.props
    return (
      <Container active={active}>
        <Header>
          {children.renderTitle()}{' '}
          <Arrow>{active ? 'keyboard_arrow_up' : 'keyboard_arrow_down'}</Arrow>
        </Header>
        {active && <Content>{children.renderBody()}</Content>}
      </Container>
    )
  }
}

export default Dropdown
