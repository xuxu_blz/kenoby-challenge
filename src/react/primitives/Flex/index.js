// @flow strict
import styled from 'styled-components'

type WrapperProps = { direction: string, justifyContent?: string }
export const Wrapper = styled.div`
  display: flex;
  flex-direction: ${({ direction }: WrapperProps) => direction};
  justify-content: ${({ justifyContent }: WrapperProps) =>
    justifyContent ? justifyContent : 'space-between'};
`

type ItemProps = {
  grow?: string,
  shrink?: string,
  basis?: string,
  maxWidth?: string,
  marginLeft?: string,
  marginRight?: string,
}

const Item = styled.div`
  flex-grow: ${({ grow }: ItemProps) => (grow ? grow : '1')};
  flex-shrink: ${({ shrink }: ItemProps) => (shrink ? shrink : '1')};
  flex-basis: ${({ basis }: ItemProps) => (basis ? basis : 'auto')};
  max-width: ${({ maxWidth }: ItemProps) => (maxWidth ? maxWidth : 'none')};
  margin-left: ${({ marginLeft }: ItemProps) =>
    marginLeft ? marginLeft : '0'};
  margin-right: ${({ marginRight }: ItemProps) =>
    marginRight ? marginRight : '0'};
`

export default { Wrapper, Item }
