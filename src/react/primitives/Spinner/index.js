// @flow strict
import React from 'react'
import styled from 'styled-components'

type Props = { size: string }

const Container = styled.div`
  width: ${({ size }) => size};
  height: ${({ size }) => size};
  margin: 0 auto;
`

const Spinner = ({ size }: Props) => (
  <Container size={size}>
    <svg
      width="100%"
      height="100%"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 100 100"
      preserveAspectRatio="xMidYMid"
    >
      <circle
        cx="50"
        cy="50"
        fill="none"
        stroke="#8400c5"
        strokeWidth="6"
        r="35"
        strokeDasharray="164.93361431346415 56.97787143782138"
      >
        <animateTransform
          attributeName="transform"
          type="rotate"
          calcMode="linear"
          values="0 50 50;360 50 50"
          keyTimes="0;1"
          dur="1s"
          begin="0s"
          repeatCount="indefinite"
        />
      </circle>
    </svg>
  </Container>
)

export default Spinner
