// @flow strict
import styled from 'styled-components'

const Center = styled.div`
  max-width: 980px;
  margin: 0 auto;
  padding: 15px;
`

export default Center
