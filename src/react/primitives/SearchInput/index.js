// @flow strict
import React from 'react'
import styled from 'styled-components'

import Icon from '../Icon'
import Flex from '../Flex'

type Props = {
  value: string,
  placeholder: string,
  onChange: (SyntheticInputEvent<HTMLInputElement>) => void,
}

const Container = styled.span`
  display: inline-block;
  width: 100%;
  background: ${({ theme }) => theme.primaryColor(3)};
  height: 36px;
  border-radius: 3px;

  i {
    margin: 0 0.4em;
  }
`

const Input = styled.input.attrs({ type: 'text' })`
  border: 0;
  background: transparent;
  height: 100%;
  color: #fff;
  width: 100%;

  &::placeholder {
    color: #fff;
    opacity: 0.8;
  }

  &:focus {
    outline: none;
  }
`

const SearchInput = ({ value, onChange, placeholder }: Props) => (
  <Container>
    <Flex.Wrapper direction="row">
      <Flex.Item grow="0" shrink="0">
        <Icon color="#fff" lineHeight="36px">
          search
        </Icon>
      </Flex.Item>

      <Flex.Item>
        <Input value={value} onChange={onChange} placeholder={placeholder} />
      </Flex.Item>
    </Flex.Wrapper>
  </Container>
)

export default SearchInput
