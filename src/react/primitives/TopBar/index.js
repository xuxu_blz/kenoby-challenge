// @flow strict
import * as React from 'react'
import styled from 'styled-components'

type Props = {
  children: React.Node,
}

const Container = styled.header`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 10;
  width: 100%;
  height: 60px;
  background: ${({ theme }) => theme.primaryColor(1)};
  box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.3);
`

const Wrapper = styled.div`
  max-width: 1100px;
  margin: 0 auto;
  padding: 12px 15px;
`

const TopBar = ({ children }: Props) => (
  <Container>
    <Wrapper>{children}</Wrapper>
  </Container>
)

export default TopBar
