// @flow strict
import React from 'react'
import styled from 'styled-components'

import SearchBar from '../../components/SearchBar'
import Positions from '../../components/Positions'
import { withResource } from '../../HOCs/withResource'
import positionsResource from '../../../resources/positions'

const ConnectedPositions = withResource(positionsResource.get, 'positions')(
  Positions
)

const PageWrapper = styled.main`
  padding-top: 80px;
`

type State = {
  searchString: string,
  showFavorites: boolean,
}

class Main extends React.Component<{}, State> {
  state = { searchString: '', showFavorites: false }

  handleUpdateSearchField = (searchString: string) => {
    this.setState({ searchString })
  }

  handleToggleFavorites = () => {
    this.setState({ showFavorites: !this.state.showFavorites })
  }

  render() {
    const { searchString, showFavorites } = this.state

    return (
      <PageWrapper>
        <SearchBar
          searchString={searchString}
          showFavorites={showFavorites}
          onUpdateSearchField={this.handleUpdateSearchField}
          onToggleFavorites={this.handleToggleFavorites}
        />

        <ConnectedPositions
          searchString={searchString}
          showFavorites={showFavorites}
        />
      </PageWrapper>
    )
  }
}

export default Main
