// @flow strict
import React from 'react'
import styled from 'styled-components'

import TopBar from '../../primitives/TopBar'
import Flex from '../../primitives/Flex'
import SearchInput from '../../primitives/SearchInput'
import Switch from '../../primitives/Switch'

const FakeLogo = styled.h1`
  background: #fff;
  width: 100px;
  height: 36px;
  text-indent: -9999em;
  overflow: hidden;
`

const FakeUser = styled.span`
  background: #fff;
  display: block;
  width: 32px;
  height: 32px;
  margin: 2px;
  border-radius: 50%;
`

type Props = {
  searchString: string,
  showFavorites: boolean,
  onToggleFavorites: () => void,
  onUpdateSearchField: string => void,
}

class SearchBar extends React.PureComponent<Props> {
  handleInputChange = ({ target }: SyntheticInputEvent<HTMLInputElement>) => {
    this.props.onUpdateSearchField(target.value)
  }

  render() {
    const { searchString, showFavorites } = this.props

    return (
      <TopBar>
        <Flex.Wrapper>
          <Flex.Item grow="0" shrink="0">
            <FakeLogo>Logo</FakeLogo>
          </Flex.Item>

          <Flex.Item grow="2" maxWidth="700px">
            <Flex.Wrapper>
              <Flex.Item>
                <SearchInput
                  value={searchString}
                  placeholder="Encontre uma vaga..."
                  onChange={this.handleInputChange}
                />
              </Flex.Item>

              <Flex.Item grow="0" shrink="0" marginLeft="16px">
                <Switch
                  active={showFavorites}
                  onClick={this.props.onToggleFavorites}
                />
              </Flex.Item>
            </Flex.Wrapper>
          </Flex.Item>

          <Flex.Item grow="0" shrink="0">
            <FakeUser />
          </Flex.Item>
        </Flex.Wrapper>
      </TopBar>
    )
  }
}

export default SearchBar
