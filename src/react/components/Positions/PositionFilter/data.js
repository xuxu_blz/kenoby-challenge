// @flow strict
export const getFavorites = (): Array<string> =>
  JSON.parse(localStorage.getItem('kenoby_favorites') || '[]')

export const storeFavorites = (favoritesList: Array<string>) => {
  localStorage.setItem('kenoby_favorites', JSON.stringify(favoritesList))
}
