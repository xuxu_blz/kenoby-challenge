// @flow strict
import * as React from 'react'
import { map, filter, pipe, prop, test, contains, without, append } from 'ramda'

import { getFavorites, storeFavorites } from './data'
import type { PositionData } from '../../../../resources/positions'

const strToLower = (string: string) => string.toLowerCase()

const addFavorite = (favoritesList: Array<string>) => (
  position: PositionData
) => ({
  ...position,
  favorite: contains(position._id, favoritesList),
})

const testTitle = (searchString: string) => (position: PositionData) =>
  pipe(prop('name'), strToLower, test(new RegExp(strToLower(searchString))))(
    position
  )

const testFavorite = (position: PositionData) => !!position.favorite

type Props = {
  positions: Array<PositionData>,
  searchString: string,
  showFavorites: boolean,
  children: (Array<PositionData>, (string) => void) => React.Node,
}

type State = { favoritesList: Array<string> }

class PositionFilter extends React.Component<Props, State> {
  state = { favoritesList: [] }

  componentDidMount() {
    this.setState({ favoritesList: getFavorites() })
  }

  handleToggleFavorite = (positionId: string) => {
    const omittedList = without([positionId], this.state.favoritesList)

    if (this.state.favoritesList.length > omittedList.length) {
      this.setState({ favoritesList: omittedList }, () => {
        storeFavorites(this.state.favoritesList)
      })
    } else {
      this.setState(
        {
          favoritesList: append(positionId, this.state.favoritesList),
        },
        () => {
          storeFavorites(this.state.favoritesList)
        }
      )
    }
  }

  render() {
    const { children, showFavorites, searchString } = this.props
    const { favoritesList } = this.state

    const positions = map(addFavorite(favoritesList))(this.props.positions)

    const filteredPositions = showFavorites
      ? pipe(filter(testTitle(searchString)), filter(testFavorite))(positions)
      : filter(testTitle(searchString), positions)

    return (
      children && (
        <React.Fragment>
          {children(filteredPositions, this.handleToggleFavorite)}
        </React.Fragment>
      )
    )
  }
}

export default PositionFilter
