// @flow strict
import React from 'react'

import Description from './Description'
import Dropdown from '../../../primitives/Dropdown'
import FavoriteToggle from '../../../primitives/FavoriteToggle'
import { ActionButtonLink } from '../../../primitives/ActionButton'
import type { PositionData } from '../../../../resources/positions'

type Props = {
  position: PositionData,
  active: boolean,
  onToggle: string => void,
  onToggleFavorite: string => void,
}

class Position extends React.Component<Props> {
  handleToggleDropdown = (event: SyntheticMouseEvent<HTMLElement>) => {
    if (event.target === event.currentTarget) {
      this.props.onToggle(this.props.position._id)
    }
  }

  handleToggleFavorite = () => {
    this.props.onToggleFavorite(this.props.position._id)
  }

  render() {
    const { position, active } = this.props

    return (
      <Dropdown active={active}>
        {{
          renderTitle: () => (
            <React.Fragment>
              <h2 onClick={this.handleToggleDropdown}>
                {position.name}{' '}
                <FavoriteToggle
                  active={position.favorite}
                  onClick={this.handleToggleFavorite}
                />
              </h2>
            </React.Fragment>
          ),
          renderBody: () => (
            <React.Fragment>
              <Description html={position.description} />

              {position.linksExternal[0] && (
                <ActionButtonLink href={position.linksExternal[0]}>
                  Candidate-se
                </ActionButtonLink>
              )}
            </React.Fragment>
          ),
        }}
      </Dropdown>
    )
  }
}

export default Position
