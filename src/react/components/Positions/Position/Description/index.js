// @flow strict
import * as React from 'react'
import sanitizeHtml from 'sanitize-html'
import styled from 'styled-components'

const Content = styled.div`
  font-size: 14px;

  h3 {
    font-size: 14px;
    margin: 0.6em 0;
  }

  p {
    margin: 0.6em 0;
  }

  ul {
    margin: 0.6em 0;
    padding-left: 2em;
    list-style: square;
  }
`

type Props = { html: string }
const Description = ({ html }: Props) => (
  <Content dangerouslySetInnerHTML={{ __html: sanitizeHtml(html) }} />
)

export default Description
