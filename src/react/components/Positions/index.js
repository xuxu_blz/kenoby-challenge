// @flow strict
import React from 'react'
import { map } from 'ramda'

import type { APIError } from '../../../types/api'
import type { PositionData } from '../../../resources/positions'
import PositionFilter from './PositionFilter'
import Position from './Position'
import Error from '../../primitives/Error'
import Spinner from '../../primitives/Spinner'
import Center from '../../primitives/Center'

const Wrapper = Center.withComponent('section')

type Props = {
  resourceError: APIError,
  positions: Array<PositionData>,
  searchString: string,
  showFavorites: boolean,
}

type State = { selectedPosition: string }

class Positions extends React.Component<Props, State> {
  state = { selectedPosition: '0' }

  handleTogglePosition = (id: string) => {
    if (this.state.selectedPosition === id) {
      this.setState({ selectedPosition: '0' })
    } else {
      this.setState({ selectedPosition: id })
    }
  }

  render() {
    const { resourceError, positions, searchString, showFavorites } = this.props

    if (resourceError) {
      return <Error>{resourceError.message}</Error>
    }

    if (!positions) {
      return <Spinner size="100px" />
    }

    const { selectedPosition } = this.state

    return (
      <Wrapper>
        <PositionFilter
          positions={positions}
          showFavorites={showFavorites}
          searchString={searchString}
        >
          {(filteredPositions, onToggleFavorite) =>
            map(position => (
              <Position
                key={position._id}
                position={position}
                active={position._id === selectedPosition}
                onToggle={this.handleTogglePosition}
                onToggleFavorite={onToggleFavorite}
              />
            ))(filteredPositions)
          }
        </PositionFilter>
      </Wrapper>
    )
  }
}

export default Positions
