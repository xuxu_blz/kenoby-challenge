// @flow strict
import React from 'react'
import type { ComponentType } from 'react'

import type { APIError } from '../../../types/api'

type Props = {}

type State = { data: mixed, error: ?APIError }

export const withResource = <T>(
  get: () => Promise<[?APIError, T]>,
  propName: string
) => <InputProps: {}>(WrappedComponent: ComponentType<InputProps>) => {
  return class withResource extends React.Component<
    $Rest<InputProps, Props>,
    State
  > {
    state = { data: null, error: null }

    componentDidMount() {
      this.fetch()
    }

    fetch = async () => {
      const [error, data] = await get()
      if (error) {
        this.setState({ error: { code: error.code, message: error.message } })
      } else {
        this.setState({ data, error: null })
      }
    }

    render() {
      const passedProps = {
        ...this.props,
        [propName]: this.state.data,
        resourceError: this.state.error,
      }

      return <WrappedComponent {...passedProps} />
    }
  }
}
