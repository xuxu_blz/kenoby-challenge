// @flow strict
import { flip, nth } from 'ramda'

const primaryColor = ['#7316C4', '#9013FE', '#AC50FD', '#AF58FC', '#C789FE']
const linkColor = ['#F00']
const danger = ['#C10016']
const yellow = ['#F2B449']
const gray = ['#ADADAD', '#D2D2D2', '#F0F0F0']

const get = (shadeList: Array<string>) => (shade: number = 0) =>
  flip(nth)(shadeList, shade)

export const theme = {
  primaryColor: get(primaryColor),
  linkColor: get(linkColor),
  danger: get(danger),
  yellow: get(yellow),
  gray: get(gray),
}
