// @flow strict
/*global process*/
import axios from 'axios'

import type { APIError } from '../types/api'

const apiToken = process.env.KENOBY_CLIENT_TOKEN || ''
const apiVersion = process.env.KENOBY_API_VERSION || ''

type CustomField = {
  code: string,
  field: string,
  value: string,
}

type JobSite = {
  _id: string,
  name: string,
  slug: string,
  picture: string,
  facebookPicture: string,
  internal: false,
}

type AvailableJobsite = {
  _id: string,
  slug: string,
}

export type PositionData = {|
  _id: string,
  name: string,
  description: string,
  customFields: Array<CustomField>,
  jobSites: Array<JobSite>,
  deadline: number,
  openedDays: number,
  jobsitesAvailable: Array<AvailableJobsite>,
  linksInternal: Array<string>,
  linksExternal: Array<string>,
  favorite?: boolean,
|}

const get = (): Promise<[?APIError, Array<PositionData>]> => {
  return new Promise(resolve => {
    axios
      .get('https://api.kenoby.com/positions', {
        headers: { 'x-tenant': apiToken, 'x-version': apiVersion },
      })
      .then(response => {
        resolve([null, response.data])
      })
      .catch(({ response }) => {
        if (response) {
          resolve([
            { code: response.status, message: 'Houve um erro no servidor' },
            [],
          ])
        } else {
          resolve([
            { code: 0, message: 'Não foi possível se conectar com o servidor' },
            [],
          ])
        }
      })
  })
}

export default { get }
