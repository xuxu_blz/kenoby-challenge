// @flow strict

export type APIError = {
  code: number,
  message: string,
}
